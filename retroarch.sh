#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

#- check internet
for i in {1..10}
do
    ping -c 1 -W ${i} www.google.com &>/dev/null && break
done

if [[ "$?" -ne 0 ]]
then
    echo "[!] Internet issues"
    exit 1
fi

if ! command -c sudo &> /dev/null
then
    apt-get -qq install -y sudo
fi

retroarch_install() {
    if ! command -v apt-get &> /dev/null
    then
	echo "[!] apt-get not found"
	exit 1
    else
    # install retroarch
	sudo apt-get -qq update && \
	    sudo apt-get -qq install -y \
		 retroarch retroarch-assets \
		 libretro-beetle-pce-fast libretro-beetle-psx \
		 libretro-beetle-vb libretro-beetle-wswan \
		 libretro-bsnes-mercury-accuracy libretro-bsnes-mercury-balanced \
		 libretro-bsnes-mercury-performance libretro-desmume \
		 libretro-gambatte libretro-genesisplusgx \
		 libretro-mgba libretro-mupen64plus \
		 libretro-nestopia libretro-snes9x
    fi
    
    if ! command -v wget &> /dev/null
    then
	echo "[!] wget not found"
	exit 1
    else
    # fox for 8bitDo controller
    # https://forums.libretro.com/t/8bitdo-gamepad-autoconfigs-udev/3036
	sudo wget https://raw.githubusercontent.com/paalfe/mixedcontent/master/udev_rules.d/99-8bitdo-bluetooth-controllers.rules -O \
	     /etc/udev/rules.d/99-8bitdo-bluetooth-controllers.rules
    fi
    #controller settings 
}
