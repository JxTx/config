#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

if ! command -c git &> /dev/null
then
    sudo apt-get -qq install -y git
fi

mkdir -p ~/.emacs.d/ && \
    git clone https://gitlab.com/JxTx/config ~/.emacs.d/ && \
    cd ~/.emacs.d/ && bash ./install.sh
