#+TITLE: Config

My config for debian/macos and emacs.

* Install
** Debian
Clone this repo to =~/.emacs.d/= and then run =./install.sh=.

#+begin_src shell
  mkdir -p ~/.emacs.d && \
      git clone https://gitlab.com/JxTx/config ~/.emacs.d/ && \
      cd ~/.emacs.d/ && bash ./install.sh
#+end_src

*** bootstrap
Or use the =bootstrap= script.

#+begin_src shell
  curl -s https://gitlab.com/JxTx/config/-/raw/master/bootstrap.sh | bash 
#+end_src

** Macos

Clone this repo to =~/.emacs.d/= and then run =./mac-config-install.sh=.

#+begin_src shell
  mkdir -p ~/.emacs.d && \
      git clone https://gitlab.com/JxTx/config ~/.emacs.d/ && \
      cd ~/.emacs.d/ && bash ./mac-config-install.sh
#+end_src
