#!/bin/bash

# install brew
if ! command -v brew &> /dev/null
   then
       /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

brew install --cask firefox
brew install --cask mactex-no-gui 
brew install --cask emacs 
brew install --cask vlc 
brew install --cask iterm2 
brew install --cask chromium 
brew install --cask virtualbox 
brew install --cask wireshark
brew install --cask ios-app-signer
brew install --cask docker 
brew install --cask provisionql 
brew install --cask ngrok
brew install --cask checkra1n 
brew install --cask android-studio 
brew install --cask android-platform-tool
brew install --cask spotify

brew install exa bat grc zsh-syntax-highlighting zsh-autosuggestions source-highlight the_silver_searcher iproute2mac \
     git tmux tmate mosh curl wget httpie sqlite3 john pandoc rdesktop gdb vagrant nmap aria2 ddgr cowsay ansible libvirt docker-compose kubectl minikube mpd mas p7zip \
     pidof hping prettyping fzf \
     ncdu rlwrap jq pwgen nuclei \
     cfr-decompiler \
     shellcheck \
     golang \
     npm nvm \
     ideviceinstaller bettercap jadx ghidra apktool libusbmuxd ios-deploy dex2jar

brew install --cask homebrew/cask-fonts/font-hasklig

if ! command -v curl &> /dev/null
then
    echo "[!] curl not found"
    exit 1
else
    sh <(curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs) -y
fi

if ! command -v pip3 &> /dev/null
then
    echo "[!] pip3 not found"
    exit 1
else
    pip3 install bandcamp-downloader && \
	pip3 install getsploit && \
	pip3 install frida-tools && \
	pip3 install objection && \
	pip3 install updog && \
	pip3 install impacket && \
	pip3 install shodan && \
	pip3 install raccoon && \
	pip3 install pyftpdlib && \
	pip3 install proselint && \
	pip3 install flake8 && \
	pip3 install sqlint && \
	pip3 install pytest && \
	pip3 install pylint && \
	pip3 install --user poetry && \
	pip3 install --user pipenv
fi

if ! command -v npm &> /dev/null
then
    echo "[!] npm not found"
    exit 1
else
    sudo npm install -g jshint && \
	sudo npm -g install js-beautify && \
	sudo npm install jsonlint -g && \
	sudo npm install --global generate generate-gitignore && \
	sudo npm install yarn -g && \
	sudo npm install eslint -g && \
	sudo npm install nodemon -g && \
	sudo npm install igf -g
fi

if [ ! -d ~/opt/ ]
then
    mkdir -p ~/opt/
fi

if [ ! -d ~/.local/bin/ ]
then
    mkdir -p  ~/.local/bin/
fi

# radare2
mkdir -p ~/opt/radare2 && \
    git clone https://github.com/radare/radare2 ~/opt/radare2 && \
    cd ~/opt/radare2 && \
    sys/install.sh && \
    pip3 install r2pipe && \
    echo "eco smyck" >> "$HOME/.radare2rc" && \
    echo "e scr.color=1" >> "$HOME/.radare2rc" && \
    cd "$HOME"

# iaito
VERSION=5.2.0
wget "https://github.com/radareorg/iaito/releases/download/${VERSION}/iaito-${VERSION}_macos.pkg" -O ~/opt/iaito-${VERSION}_macos.pkg

# r2pm
r2pm init && \
    r2pm -ci r2frida && \
    r2pm -ci r2ghidra

# gef-gdb
wget -q -O- https://github.com/hugsy/gef/raw/master/scripts/gef.sh | sh

# dsdump
wget https://github.com/DerekSelander/dsdump/blob/master/compiled/dsdump_compiled.zip -O ~/opt/dsdump_compiled.zip

# frida-ios-dump
mkdir ~/opt/frida-ios-dump && \
    git clone https://github.com/AloneMonkey/frida-ios-dump ~/opt/frida-ios-dump

# SecLists
mkdir -p ~/opt/SecLists && \
    git clone https://github.com/danielmiessler/SecLists ~/opt/SecLists

# PayloadsAllTheThings
mkdir -p ~/opt/PayloadsAllTheThings && \
    git clone https://github.com/swisskyrepo/PayloadsAllTheThings ~/opt/PayloadsAllTheThings

# OWASP CheatSheets
mkdir ~/opt/CheatSheetSeries && \
    git clone https://github.com/OWASP/CheatSheetSeries ~/opt/CheatSheetSeries

# dirsearch
mkdir -p ~/opt/dirsearch && \
    git clone https://github.com/maurosoria/dirsearch ~/opt/dirsearch

# graudit
mkdir -p ~/opt/graudit && \
    git clone https://github.com/wireghoul/graudit ~/opt/graudit

# lldb wrappers
mkdir -p ~/opt/LLDB && \
    git clone git clone https://github.com/DerekSelander/LLDB ~/opt/LLDB

# FridaAndroidTracer
mkdir -p ~/opt/FridaAndroidTracer && \
    git clone https://github.com/Piasy/FridaAndroidTracer ~/opt/FridaAndroidTracer

# IPAPatch
mkdir -p ~/opt/IPAPatch && \
    git clone https://github.com/Naituw/IPAPatch ~/opt/IPAPatch

# ghidr-dark-theme
mkdir -p ~/opt/ghidra-dark && \
    git clone https://github.com/zackelia/ghidra-dark ~/opt/ghidra-dark

# AvaloniaILSpy
VERSION=5.0-rc2
wget "https://github.com/icsharpcode/AvaloniaILSpy/releases/download/v${VERSION}/ILSpy-osx-x64-Release.zip" -O ~/opt/ILSpy-osx-x64-Release.zip && \
    cd ~/opt/ && \
    unzip -d ILSpy-osx-x64 ./ILSpy-osx-x64-Release.zip && \
    cp -r ~/opt/ILSpy-osx-x64/ILSpy.app /Applications && \
    chmod +x "/Applications/ILSpy.app/Contents/MacOS/ILSpy"

# keychain-dumper
VERSION=1.0.0
wget "https://github.com/ptoomey3/Keychain-Dumper/releases/download/${VERSION}/keychain_dumper-${VERSION}.zip" -O ~/opt/keychain_dumper-${VERSION}.zip

if ! command -v go &> /dev/null
then
    echo "[!] go not found"
    exit 1
else
    # gocode
    go get -u github.com/nsf/gocode
    
    # gobuster
    go get github.com/OJ/gobuster
    
    # ffuf
    go get github.com/ffuf/ffuf
    
    # headi
    go get github.com/mlcsec/headi
fi

# zsh fixes
sudo chmod -R 755 /usr/local/share/zsh/site-functions
sudo chmod -R 755 /usr/local/share/zsh

if [ ! -d ~/zfunc/ ]
then
    mkdir -p  ~/.zfunc/
fi

# python stuff
#PIPX=$(command -v pipx)
PIPENV=$(command -v pipenv)
POETRY=$(command -v poetry)

#"$PIPX" install mycli && \
    #    "$PIPX" install pgcli && \
    #"$PIPX install dwn" && \
    # "$PIPX install bloodhound"
"$POETRY" completions zsh > ~/.zfunc/_poetry && \
    "$PIPENV" --completion > ~/.zfunc/_pipenv

## pylintrc
FILE=~/.pylintrc
cat <<EOF > $FILE
[MESSAGES CONTROL]

disable=missing-docstring

EOF

## jshintrc
FILE=~/.jshintrc
cat <<EOF > $FILE
{
  "esversion": 6
}

EOF


# lldb
FILE=~/.lldbinit
cat <<EOF > $FILE
command script import ~/opt/LLDB/lldb_commands/dslldb.py
EOF

# zsh
FILE=~/.zshrc
touch $FILE
cat <<EOF > $FILE
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000000
SAVEHIST=10000000
setopt appendhistory
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/Users/jthorpe/.zshrc'
fpath+=~/.zfunc

if type brew &>/dev/null; then
  FPATH=\$(brew --prefix)/share/zsh/site-functions:\$FPATH

  autoload -Uz compinit
  compinit
fi

#autoload -Uz compinit
#compinit
autoload -U bashcompinit
bashcompinit
# End of lines added by compinstall

source ~/.abbr_pwd
precmd() {
    export PS1="%F{8}[%F{4}\$(felix_pwd_abbr)%F{8}]%% %f"
}

#export PS1="[%F{yellow}%~%f]%# "
#export PS1="%F{8}[%F{4}%~%F{8}]%% %f"
export GOPATH=/Users/jthorpe/go/
export PATH=/Users/jthorpe/.nvm/versions/node/v14.16.0/bin:/Users/jthorpe/.pyenv/shims:/Users/jthorpe/.pyenv/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/Users/jthorpe/.local/bin:/Users/jthorpe/go/bin:/usr/local/Cellar/python@3.9/3.9.2_1/Frameworks/Python.framework/Versions/3.9/bin:/Users/jthorpe/Library/Python/3.9/bin:/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin:/Library/Apple/usr/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/Users/jthorpe/.local/bin:/Users/jthorpe/go/bin:/Users/jthorpe/go/bin:/usr/local/Cellar/python@3.9/3.9.2_1/Frameworks/Python.framework/Versions/3.9/bin
export MANPAGER="sh -c 'col -bx | bat --theme Nord -l man -p'"
export LESS='-i -M -R -w -z-4'
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source ~/.functions
source ~/.aliases
[[ -s "/etc/grc.zsh" ]] && source /etc/grc.zsh
#eval "\$(register-python-argcomplete pipx)"
EOF

chmod -R go-w "$(brew --prefix)/share"

echo "export GOPATH=/Users/jthorpe/go/" >> ~/.profile
echo "export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/Users/jthorpe/.local/bin:/Users/jthorpe/go/bin:/Users/jthorpe/go/bin:/usr/local/Cellar/python@3.9/3.9.2_1/Frameworks/Python.framework/Versions/3.9/bin" >> ~/.profile

wget https://raw.githubusercontent.com/felixgravila/zsh-abbr-path/master/.abbr_pwd -O "$HOME/.abbr_pwd"

FILE=~/.aliases
touch $FILE
cat <<EOF > $FILE
#!/bin/bash
alias emacsclient='emacsclient -s ~/.emacs.d/server/server '
alias ec='emacsclient -s ~/.emacs.d/server/server '
alias ls='exa --long --header --git '
alias grep='ag --color-line-number 1 --color-path 34 --color-match 44 '
alias ipinfo="curl ifconfig.me"
alias pyhttpd='python3 -m http.server'
alias pyftpd='python3 -m pyftpdlib'
#alias nmap='grc nmap'
alias ping='prettyping --nolegend'
#alias docker='grc docker'
alias cat='bat -pp --theme Nord '
alias less='bat --theme Nord '
#\$(brew --prefix)/opt/fzf/install
alias preview="fzf --preview 'bat --color \"always\" --theme Nord {}'"

EOF

# GRC functions
sudo wget https://raw.githubusercontent.com/garabik/grc/master/grc.bashrc -O /etc/grc.bashrc
sudo wget https://raw.githubusercontent.com/garabik/grc/master/grc.zsh -O /etc/grc.zsh

FILE=~/.functions
touch $FILE
cat <<EOF > $FILE
#!/bin/bash

smbserver(){
	sudo python3 \
	/usr/local/Cellar/python@3.9/3.9.2_1/Frameworks/Python.framework/Versions/3.9/bin/smbserver.py \
	share . -smb2support
}

pullallthethings(){
    /bin/ls | xargs -I{} git -C {} pull
}

gi() { curl -sLw n https://www.toptal.com/developers/gitignore/api/\$\@ ;}

encrypt() {
    local FILE=\$1
    tar cz "\$FILE" | openssl enc -aes-256-cbc -e -v > "\$FILE.tar.gz.enc"
}

magit(){
        if [ -d "\$1" ]; then
            cd "\$1"
        fi
        emacsclient -s ~/.emacs.d/server/server --eval "(magit)" --no-wait
}

EOF


#tmux
mkdir -p ~/.tmux/plugins/tpm && \
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
##
cat <<EOF > ~/.tmux.conf
set-option -g status-interval 1
set-option -g automatic-rename on
set-option -g automatic-rename-format '#{b:pane_current_path}'

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com:user/plugin'
# set -g @plugin 'git@bitbucket.com:user/plugin'

# nord theme
set -g @plugin "arcticicestudio/nord-tmux"

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
EOF

wget https://raw.githubusercontent.com/arcticicestudio/nord-iterm2/develop/src/xml/Nord.itermcolors -O ~/.config/iterm2/Nord.itermcolors

mkdir -p ~/Library/Developer/Xcode/UserData/FontAndColorThemes && \
    curl https://raw.githubusercontent.com/arcticicestudio/nord-xcode/develop/src/Nord.xccolortheme -o ~/Library/Developer/Xcode/UserData/FontAndColorThemes/Nord.xccolortheme

# vagrant plugins
vagrant plugin install vagrant-timezone

# pdflatex
cd /Library/TeX/texbin && \
    sudo ./tlmgr update --self && \
    sudo ./tlmgr install wrapfig && \
    sudo ./tlmgr install capt-of
